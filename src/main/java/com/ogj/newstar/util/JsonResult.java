package com.ogj.newstar.util;

import java.io.Serializable;

/**
 * 用于封装服务器返回的数据
 * @param <T>
 */

public class JsonResult<T> implements Serializable {
    private String msg;
    private String state;
    private T data;

    public JsonResult() {
    }

    @Override
    public String toString() {
        return "JsonResult{" +
                "msg='" + msg + '\'' +
                ", state='" + state + '\'' +
                ", data=" + data +
                '}';
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public JsonResult(String msg, String state, T data) {
        this.msg = msg;
        this.state = state;
        this.data = data;
    }
}
