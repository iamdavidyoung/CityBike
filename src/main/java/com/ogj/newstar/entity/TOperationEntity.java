package com.ogj.newstar.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ouguangji
 * @email sunny_ogj_mail@163.com
 * @date 2020-07-19 09:12:40
 * 实体类，用户操作实体
 */
@Data
@TableName("t_operation")
@NoArgsConstructor
@AllArgsConstructor
public class TOperationEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 操作类型，1-拖拽，2-点击，3-滚轮缩放
     */
    private Integer optType;
    /**
     * 用户ip地址
     */
    private String userIp;
    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createdTime;
    /**
     * 点击的站点id
     */
    private Integer stationId;
    /**
     * 开始拖拽的纬度
     */
    private Double dragStartLat;
    /**
     * 开始拖拽的经度
     */
    private Double dragStartLon;
    /**
     * 拖拽后的纬度
     */
    private Double dragEndLat;
    /**
     * 拖拽后的经度
     */
    private Double dragEndLon;
    /**
     * 开始放缩的等级
     */
    private Integer zoomStartLevel;
    /**
     * 放缩后的等级
     */
    private Integer zoomEndLevel;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TOperationEntity that = (TOperationEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(optType, that.optType) &&
                Objects.equals(userIp, that.userIp) &&
                Objects.equals(createdTime, that.createdTime) &&
                Objects.equals(stationId, that.stationId) &&
                Objects.equals(dragStartLat, that.dragStartLat) &&
                Objects.equals(dragStartLon, that.dragStartLon) &&
                Objects.equals(dragEndLat, that.dragEndLat) &&
                Objects.equals(dragEndLon, that.dragEndLon) &&
                Objects.equals(zoomStartLevel, that.zoomStartLevel) &&
                Objects.equals(zoomEndLevel, that.zoomEndLevel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, optType, userIp, createdTime, stationId, dragStartLat, dragStartLon, dragEndLat, dragEndLon, zoomStartLevel, zoomEndLevel);
    }
}
