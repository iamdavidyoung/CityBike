package com.ogj.newstar.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;


@Data
@TableName("t_station_info")
public class TStationEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 站点id
     */
    @TableId
    private Integer stationId;
    /**
     * 站点名称
     */
    private String name;
    /**
     * 站点纬度
     */
    private Double lat;
    /**
     * 站点经度
     */
    private Double lon;

}
