package com.ogj.newstar;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;


/**
 * @author Davidn young
 */
@ServletComponentScan
@MapperScan
@SpringBootApplication
public class NewstarNybikeApplication {

    public static void main(String[] args) {
        SpringApplication.run(NewstarNybikeApplication.class, args);
    }

}
