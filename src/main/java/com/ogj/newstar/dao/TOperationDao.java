package com.ogj.newstar.dao;

import com.ogj.newstar.entity.TOperationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 
 * 
 * @author ouguangji
 * @email sunny_ogj_mail@163.com
 * @date 2020-07-18 12:13:25
 */
@Mapper
public interface TOperationDao extends BaseMapper<TOperationEntity> {
	Integer insertOperation(TOperationEntity operationEntity);

	/**
	 * 查询所有的操作信息
	 */
	List<TOperationEntity> OperationList();
}
