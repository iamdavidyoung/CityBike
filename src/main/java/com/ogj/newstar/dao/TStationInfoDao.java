package com.ogj.newstar.dao;

import com.ogj.newstar.entity.TStationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 
 * 
 * @author ouguangji
 * @email sunny_ogj_mail@163.com
 * @date 2020-07-20 11:11:05
 */
@Mapper
public interface TStationInfoDao extends BaseMapper<TStationEntity> {
    TStationEntity selectById(Integer id);

    TStationEntity selectByName(String name);

    List<TStationEntity> getStationList();
}
