package com.ogj.newstar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;

public class ServletInitializer extends SpringBootServletInitializer {

    protected static final Logger logger = LoggerFactory.getLogger(NewstarNybikeApplication.class);

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        System.out.println("---开始---");
        //写一个启动时间
        logger.debug("===================================================================启动================================================");
        try {
            logger.debug("=================时间:"+new Date().toString()+", IP:"+ InetAddress.getLocalHost().getHostAddress()+"====================");
            logger.debug("=======================================================================================================================");
        } catch (UnknownHostException e) {
            logger.warn("========================================获取ip失败:"+e.toString()+"=======================");
            e.printStackTrace();
        }
        return application.sources(NewstarNybikeApplication.class);
    }

}
