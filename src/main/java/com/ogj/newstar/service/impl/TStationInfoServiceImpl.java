package com.ogj.newstar.service.impl;

import com.ogj.newstar.dao.TStationInfoDao;
import com.ogj.newstar.entity.TStationEntity;
import com.ogj.newstar.service.TStationInfoService;
import com.ogj.newstar.service.ex.EmptyArgumentException;
import org.omg.IOP.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("tStationInfoService")
public class TStationInfoServiceImpl implements TStationInfoService {

    @Autowired
    TStationInfoDao tStationInfoDao;

    @Override
    public TStationEntity FindStationById(Integer id) {
        if(id==null){
            throw new EmptyArgumentException("查询操作记录异常:操作记录id为空");
        }
        return tStationInfoDao.selectById(id);
    }

    @Override
    public List<TStationEntity> getStationList() {
        return tStationInfoDao.getStationList();
    }
}
