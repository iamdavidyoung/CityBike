package com.ogj.newstar.service;

import com.ogj.newstar.entity.TStationEntity;

import java.util.List;

public interface TStationInfoService {

    TStationEntity FindStationById(Integer id);

    List<TStationEntity> getStationList();
}
