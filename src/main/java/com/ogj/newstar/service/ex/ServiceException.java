package com.ogj.newstar.service.ex;

/**
 * 自定义返回异常的父类
 *
 *
 * 自定义异常的开发步骤：
 * 1、声明一个类，以Exception结尾
 * 2、继承Exception(编译异常) 或者 RuntimeException(运行时异常)
 * 3、添加所有的构造器，并保证构造器中调用了父类的构造器
 */
public class ServiceException extends RuntimeException{


    public ServiceException() {
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }

    public ServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
