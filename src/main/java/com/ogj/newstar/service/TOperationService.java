package com.ogj.newstar.service;

import com.baomidou.mybatisplus.extension.service.IService;

import com.ogj.newstar.entity.TOperationEntity;
import com.ogj.newstar.entity.TStationEntity;
import com.ogj.newstar.util.PageUtils;
import com.ogj.newstar.vo.OperationVo;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author ouguangji
 * @email sunny_ogj_mail@163.com
 * @date 2020-07-18 12:13:25
 */
public interface TOperationService extends IService<TOperationEntity> {

    PageUtils queryPage(Map<String, Object> params);


    /**
     * 添加一条用户操作记录
     * @param
     */
    void saveOperation(TOperationEntity operationEntity);

    /**
     * 查询所有的操作的个数
     */
    List<TOperationEntity> SelectOperationList();

    /**
     * 计算观察数据
     */
    OperationVo getOperationViewObjectList(Integer topNumber);

    TStationEntity selectByName(String name);
}

