package com.ogj.newstar.controller;


import com.ogj.newstar.entity.TStationEntity;
import com.ogj.newstar.service.TOperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class Bkcontroller {

    @Autowired
    TOperationService tOperationService;

    @RequestMapping("/station/status")
    public String getDataStatus(HttpServletRequest req, HttpServletResponse rep) {
        ServletContext servletContext = req.getServletContext();
        String infoData= (String) servletContext.getAttribute("statusData");
        //通知浏览器我们返回的是JSON数据
        rep.setContentType("application/json;charset=utf-8");
        if(StringUtils.isEmpty(infoData)){
            return "{data:{stations:[]}}";
        }
        return infoData;
    }

    @RequestMapping("/station/info")
    public String getDataInfo(HttpServletRequest req, HttpServletResponse rep) {
        ServletContext servletContext = req.getServletContext();
        String statusData= (String) servletContext.getAttribute("infoData");
        //通知浏览器我们返回的是JSON数据
        rep.setContentType("application/json;charset=utf-8");
        if(StringUtils.isEmpty(statusData)){
            return "{data:{stations:[]}}";
        }
        return statusData;
    }
    @RequestMapping("/station/{name}")
    public TStationEntity selectStationByName(@PathVariable("name") String name){
        TStationEntity stationEntity = tOperationService.selectByName(name);
        return stationEntity;
    }
}
