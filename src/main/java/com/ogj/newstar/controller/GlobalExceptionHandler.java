package com.ogj.newstar.controller;

import com.ogj.newstar.service.ex.EmptyArgumentException;
import com.ogj.newstar.service.ex.ServiceException;
import com.ogj.newstar.util.JsonResult;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 用于处理全局异常的类
 * 原理是使用的AOP 切面编程
 */
@ControllerAdvice
@ResponseBody
public class GlobalExceptionHandler {

    /**
     * 统一处理业务层自定义异常的方法
     * @param e
     * @return
     */
    @ExceptionHandler(ServiceException.class)
    public JsonResult<Void> handlerServiceException(Throwable e){
        JsonResult<Void> jsonResult = new JsonResult<Void>();
        //封装异常提示消息
        jsonResult.setMsg(e.getMessage());
        //根据异常类型，设定状态码
        if(e instanceof EmptyArgumentException){
            jsonResult.setState("201");
        }
        else if (e instanceof InstantiationException){
            jsonResult.setState("301");
        }
        //返回jr对象
        return jsonResult;
    }

}
