package com.ogj.newstar.controller.T;

import java.util.Arrays;
import java.util.Map;

import com.ogj.newstar.util.PageUtils;
import com.ogj.newstar.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ogj.newstar.entity.TOperationEntity;
import com.ogj.newstar.service.TOperationService;




/**
 * 
 *
 * @author ouguangji
 * @email sunny_ogj_mail@163.com
 * @date 2020-07-18 12:13:25
 */
@RestController
@RequestMapping("newstar/toperation")
public class TOperationController {
    @Autowired
    private TOperationService tOperationService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = tOperationService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
		TOperationEntity tOperation = tOperationService.getById(id);

        return R.ok().put("tOperation", tOperation);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody TOperationEntity tOperation){
		tOperationService.save(tOperation);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody TOperationEntity tOperation){
		tOperationService.updateById(tOperation);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
		tOperationService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
