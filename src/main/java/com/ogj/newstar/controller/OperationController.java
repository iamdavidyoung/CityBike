package com.ogj.newstar.controller;


import com.ogj.newstar.entity.TOperationEntity;
import com.ogj.newstar.service.TOperationService;
import com.ogj.newstar.util.IPUtils;
import com.ogj.newstar.util.JsonResult;
import com.ogj.newstar.vo.OperationVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/operation")
public class OperationController {

    @Autowired
    TOperationService tOperationService;




    @RequestMapping("/save")
    public JsonResult<Void> saveOperation(@RequestBody TOperationEntity tOperationEntity){
        //1、获取用户的ip
        String ipAddr = IPUtils.getIpAddr();
        //2、将ip导入到用户中
        tOperationEntity.setUserIp(ipAddr);
        //3、调用业务层代码
        tOperationService.saveOperation(tOperationEntity);
        //4、根据结果返回不同的状态码
        return new JsonResult<Void>("200","OK",null);
    }

    @RequestMapping("/info/{top_number}")
    public JsonResult<OperationVo> infoOperation(@PathVariable("top_number") Integer topnumber){
        OperationVo viewObjectList = tOperationService.getOperationViewObjectList(topnumber);
        return new JsonResult<>("200","OK",viewObjectList);
    }
//    sse方法

    @RequestMapping("/sse/{top_number}")
    public SseEmitter subscribe(@PathVariable("top_number") Integer top_number){
        OperationVo viewObjectList = tOperationService.getOperationViewObjectList(top_number);
        //创建SseEmitter 对象
        SseEmitter sseEmitter = new SseEmitter(24 * 60 * 60 * 1000L);
        //启动子线程，周期性获取大屏所需数据，调用SseEmitter推送数据
        Long interval = 30L;
        MyRunner myRunner=new MyRunner(sseEmitter,top_number,interval);
        Thread thread=new Thread(myRunner);
        thread.start();
        //为sseEmitter设置OnCompleation时间
        sseEmitter.onCompletion(()->{
            System.out.println(Thread.currentThread().getName()+"===>SSE终止----");
            myRunner.stop();;
        });
        return sseEmitter;
    }
    class MyRunner implements Runnable{
        private SseEmitter this_sseEmitter;
        private Integer top_number;
        private Long interval=10L;
        private volatile boolean flag=true;  //循环的开关
        public MyRunner(SseEmitter sseEmitter,Integer top_number,Long interval){
            this.this_sseEmitter=sseEmitter;
            this.top_number=top_number;
            this.interval = interval;
        }
        public void stop(){
            this.flag=false;
        }
        @Override
        public void run() {
            //启动子线程，周期性获取大屏所需数据，调用SseEmitter推送数据
            while(flag) {
                try {
                    TimeUnit.SECONDS.sleep(interval);
                    this_sseEmitter.send(tOperationService.getOperationViewObjectList(top_number));
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
