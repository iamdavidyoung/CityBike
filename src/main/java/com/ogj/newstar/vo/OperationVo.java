package com.ogj.newstar.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.Map;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class OperationVo implements Serializable {
    /**
     * 地图访问量
     */
    private Integer operationSum;
    /**
     * 拖拽操作数量
     */
    private Integer operationDragSum;
    /**
     * 缩放操作数量
     */
    private Integer operationLevelAdjustSum;
    /**
     * 站点被点击操作的数量
     * 每个站点name的被点击的数量
     */
    private Map<String,Long> operationStationClick;
    /**
     * 排名前5的被点击站点的名称
     */
    private List<String> TopFiveStationName;
    /**
     * 排名前5的被点击站点的数量
     */
    private List<Long> TopFiveStationNumber;
    /**
     * 操作最多的前5名用户的ip
     */
    private List<String> TopUserIp;
    /**
     * 所有的用户ip数
     */
    private Long userIpSum;

    /**
     * 对于每类操作的数量
     */
//    private List<Map<String,List<Map.Entry<Integer,Long>>>> station_type_List;

    /**
     * 每类操作数量分布
     */
    private Map<Integer,Long> operation_type_count;


}
