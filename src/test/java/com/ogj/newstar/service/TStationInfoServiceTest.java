package com.ogj.newstar.service;

import com.ogj.newstar.entity.TStationEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TStationInfoServiceTest {

    @Autowired
    TStationInfoService tStationInfoService;

    @Test
    void FindStationById(){
        TStationEntity entity = tStationInfoService.FindStationById(72);
        System.out.println(entity.toString());
    }

}
