package com.ogj.newstar.service;


import com.ogj.newstar.entity.TOperationEntity;
import com.ogj.newstar.vo.OperationVo;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@SpringBootTest
public class TOperationServiceTest {

    @Autowired
    TOperationService tOperationService;

    @Test
    public void saveOperation() throws ParseException {
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");

        TOperationEntity operationEntity = new TOperationEntity(null,1,"127.0.0.2",sdf.parse(sdf.format(new Date())),789,127.77,43.12,12.78,42.6,13,12);

        tOperationService.saveOperation(operationEntity);
    }

    @Test
    public void SelectOperationList(){
        List<TOperationEntity> entityList = tOperationService.SelectOperationList();
        entityList.forEach(System.out::println);
    }

    @Test
    public void getOperationViewObjectList(){
        OperationVo operationViewObjectList = tOperationService.getOperationViewObjectList(10);
        System.out.println(operationViewObjectList.toString());
    }
}
