package com.ogj.newstar.dao;


import com.ogj.newstar.entity.TOperationEntity;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@SpringBootTest
public class TOperationDaoTest {
    @Autowired
    TOperationDao tOperationDao;

    @Test
    public void insertOperation() throws ParseException {
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");

        TOperationEntity operationEntity = new TOperationEntity(5L,1,"127.0.0.1",sdf.parse(sdf.format(new Date())),789,127.77,43.12,12.78,42.6,13,12);
        System.out.println(operationEntity.toString());
        System.out.println(tOperationDao.insertOperation(operationEntity));
    }

    @Test
    public void OperationList(){
        List<TOperationEntity> entities = tOperationDao.OperationList();
        entities.forEach(System.out::println);
    }
}
