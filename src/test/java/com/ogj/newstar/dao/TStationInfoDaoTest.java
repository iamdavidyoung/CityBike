package com.ogj.newstar.dao;

import com.ogj.newstar.entity.TStationEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TStationInfoDaoTest {

    @Autowired
    TStationInfoDao tStationInfoDao;

    @Test
    void selectById(){
        TStationEntity entity = tStationInfoDao.selectById(72);
        System.out.println(entity.toString());
    }
}
