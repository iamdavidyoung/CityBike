package com.ogj.newstar;

import com.ogj.newstar.dao.TOperationDao;
import com.ogj.newstar.entity.TOperationEntity;
import com.ogj.newstar.util.HttpUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@SpringBootTest
class NewstarNybikeApplicationTests {


    @Test
    void contextLoads() {
        String infoUrl="https://gbfs.citibikenyc.com/gbfs/en/station_information.json";
        String statusUrl="https://gbfs.citibikenyc.com/gbfs/en/station_status.json";
        for (int i = 1; i <=10 ; i++) {
            new Thread(()->{
                String s = HttpUtils.get(infoUrl);
                System.out.println(s);
            },String.valueOf(i)).start();
        }

    }

    public static void main(String[] args) throws IOException {
//        String infoUrl="https://gbfs.citibikenyc.com/gbfs/en/station_information.json";
//        String statusUrl="https://gbfs.citibikenyc.com/gbfs/en/station_status.json";
//        for (int i = 1; i <=100 ; i++) {
//            new Thread(()->{
//                String s = HttpUtils.get(infoUrl);
//                System.out.println("=========================");
//                System.out.println(Thread.currentThread().getName()+","+s.length());
//                System.out.println("==========================");
//            },String.valueOf(i)).start();
//        }


        String infoUrl="https://gbfs.citibikenyc.com/gbfs/en/station_information.json";
        String statusUrl="https://gbfs.citibikenyc.com/gbfs/en/station_status.json";
        for (int i = 1; i <=10 ; i++) {
            new Thread(()->{
                String s = HttpUtils.get(infoUrl);
                System.out.println(Thread.currentThread().getName()+",获取："+s.length());
            },String.valueOf(i)).start();
        }
    }

}
