# nybike

#### 介绍
这是一个基于纽约共享单车用户的大数据数据分析项目，基于springboot、echarts、百度地图api、Hadoop、hive等技术

#### 软件架构
软件架构说明


#### 安装教程

1.   **SpringBoot 2.1.15.RELEASE** 
2.   **Maven 3.6.3** 
3.   **百度地图api** 

#### 使用说明

1.  下载压缩包，并使用idea打开
2.  加载完所有依赖
3.  点击运行，访问  **`http://localhost:9001/`**  即可


#### 项目效果：

用户使用界面：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0721/220453_03f17c11_7397701.png "屏幕截图.png")

访问地址：`http://47.97.192.241:8080/nybike/`

用户数据收集分析大屏：

![输入图片说明](https://images.gitee.com/uploads/images/2020/0721/220509_0577d869_7397701.png "屏幕截图.png")

访问地址：`http://47.97.192.241:8080/nybike/dataView.html`




#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
